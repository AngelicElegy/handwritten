function throttle(fun,delay){
    let timer=null;
    return function(){
        // 发现已经开了一个,那就后续的就不予受理
        if(timer) return;
        timer=setTimeout(()=>{
            fun.apply(this,arguments)
            // 执行完一定要置空
            timer=null;
        },delay)
    }
}