function debounce (fun,delay){
    let timer=null
    return function (){
        // 函数执行时，直接清除之前开的所有定时器
        clearTimeout(timer)
        // 重开
        timer=setTimeout(()=>{
            fun.apply(this,arguments)
        },delay)
    }
}