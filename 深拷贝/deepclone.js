function deepClone(obj) {
    let newObj = obj instanceof Array ? [] : {}
    for (let key in obj) {
        let item=obj[key]
        newObj[key] = isComplex(item)?deepClone(item):item
    }
    return newObj
}
// 使用typeOf的特殊机制去判断 复杂数据类型
let isComplex = (val) => typeof val === "object" && val !== null