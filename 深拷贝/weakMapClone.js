function WeakMapDeepClone (obj,wMap=new WeakMap()){
    if(isObj(obj)){
        // 复杂数据类型
        let newObj=Array.isArray(obj)?[]:{}
        if(wMap.has(obj)){
            return wMap.get(obj)
        }
        wMap.set(obj,newObj)
        // Reflect.ownKeys可以获取到所有的键名,包括不可枚举
        Reflect.ownKeys.forEach(item => {
            newObj[item]=isObj(obj[item])?WeakMapDeepClone(obj[item],wMap):obj[item]
        });
        return newObj
    }else{
        // 简单数据类型
        return obj
    }
}
let isObj=(val)=>typeof val==='object'&&val!==null